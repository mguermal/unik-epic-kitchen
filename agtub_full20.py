

#RUI: Here we starts defining all the important part to the GCN modules which is taken directely from UNIK github repo




import math

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable


def import_class(name):
    components = name.split('.')
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


def conv_branch_init(conv, branches):
    weight = conv.weight
    n = weight.size(0)
    k1 = weight.size(1)
    k2 = weight.size(2)
    nn.init.normal_(weight, 0, math.sqrt(2. / (n * k1 * k2 * branches)))
    nn.init.constant_(conv.bias, 0)


def conv_init(conv):
    nn.init.kaiming_normal_(conv.weight, mode='fan_out')
    nn.init.constant_(conv.bias, 0)


def bn_init(bn, scale):
    nn.init.constant_(bn.weight, scale)
    nn.init.constant_(bn.bias, 0)


class UnfoldTemporalWindows(nn.Module):
    def __init__(self, window_size, window_stride=1, window_dilation=1):
        super().__init__()
        self.window_size = window_size
        self.window_stride = window_stride
        self.window_dilation = window_dilation

        self.padding = (window_size + (window_size - 1) * (window_dilation - 1) - 1) // 2
        self.unfold = nn.Unfold(kernel_size=(self.window_size, 1),
                                dilation=(self.window_dilation, 1),
                                stride=(self.window_stride, 1),
                                padding=(self.padding, 0))

    def forward(self, x):
        # Input shape: (N,C,T,V), out: (N,C,T,V*tau)
        N, C, T, V = x.shape
        x = self.unfold(x)
        x = x.view(N, C, self.window_size, -1, V).permute(0, 1, 3, 2, 4).contiguous()
        x = x.view(N, C, -1, self.window_size * V)
        return x


# Temporal unit
class T_LSU(nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size=9, stride=1, dilation=1, autopad=True):
        super(T_LSU, self).__init__()
        if autopad:
            pad = int((kernel_size - 1) * dilation // 2)
        else:
            pad = 0

        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=(kernel_size, 1), padding=(pad, 0),
                              stride=(stride, 1), dilation=(dilation, 1))

        self.bn = nn.BatchNorm2d(out_channels)
        self.relu = nn.ReLU()
        conv_init(self.conv)
        bn_init(self.bn, 1)

    def forward(self, x):
        x = self.bn(self.conv(x))
        return x


# Spatial unit
#RUI: Here we extract adj matrix to use it later to predict the verbs
class S_LSU(nn.Module):
    def __init__(self, in_channels, out_channels, num_joints, tau=1, num_heads=8, coff_embedding=4, bias=True):
        super(S_LSU, self).__init__()
        inter_channels = out_channels // coff_embedding
        self.inter_c = inter_channels
        self.out_channels = out_channels
        self.tau = tau
        self.num_heads = num_heads
        self.DepM = nn.Parameter(torch.Tensor(num_heads, num_joints * tau, num_joints * tau))
        if bias:
            self.bias = nn.Parameter(torch.Tensor(num_joints * tau))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

        # Temporal window
        if tau != 1:
            self.tw = UnfoldTemporalWindows(window_size=tau, window_stride=1, window_dilation=1)
            self.out_conv = nn.Conv3d(out_channels, out_channels, kernel_size=(1, tau, 1))
            self.out_bn = nn.BatchNorm2d(out_channels)
        # Attention
        self.conv_a = nn.ModuleList()
        self.conv_b = nn.ModuleList()
        self.conv_d = nn.ModuleList()
        for i in range(self.num_heads):
            self.conv_a.append(nn.Conv2d(in_channels, inter_channels, 1))
            self.conv_b.append(nn.Conv2d(in_channels, inter_channels, 1))
            self.conv_d.append(nn.Conv2d(in_channels, out_channels, 1))

        if in_channels != out_channels:
            self.down = nn.Sequential(
                nn.Conv2d(in_channels, out_channels, 1),
                nn.BatchNorm2d(out_channels)
            )
        else:
            self.down = lambda x: x

        self.bn = nn.BatchNorm2d(out_channels)
        self.soft = nn.Softmax(-2)
        self.relu = nn.ReLU()

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                conv_init(m)
            elif isinstance(m, nn.BatchNorm2d):
                bn_init(m, 1)
        bn_init(self.bn, 1e-6)
        for i in range(self.num_heads):
            conv_branch_init(self.conv_d[i], self.num_heads)

    def reset_parameters(self) -> None:
        nn.init.kaiming_uniform_(self.DepM, a=math.sqrt(5))
        if self.bias is not None:
            fan_in, _ = nn.init._calculate_fan_in_and_fan_out(self.DepM)
            bound = 1 / math.sqrt(fan_in)
            nn.init.uniform_(self.bias, -bound, bound)

    def forward(self, x):
        if self.tau != 1:
            x = self.tw(x)
        N, C, T, V = x.size()
        print(x.size())
        
        W = self.DepM
        B = self.bias
        y = None
        A0 = None
        for i in range(self.num_heads):
            A1 = self.conv_a[i](x).permute(0, 3, 1, 2).contiguous().view(N, V, self.inter_c * T)
            A2 = self.conv_b[i](x).view(N, self.inter_c * T, V)
            A1 = self.soft(torch.matmul(A1, A2) / A1.size(-1))  # N tV tV

            A1 = W[i] + A1
            A0 = torch.cat((A0,A1.unsqueeze(1)),1) if A0 is not None else A1.unsqueeze(1) #RUI: stacking the adj matrix of multiple heads
            A2 = x.view(N, C * T, V)
            z = self.conv_d[i]((torch.matmul(A2, A1)).view(N, C, T, V))
            y = z + y if y is not None else z

        y = self.bn(y)
        y += self.down(x)
        A0 = A0.mean(1).view(N,-1)
        if self.tau == 1:
            return self.relu(y).view(N, -1, T, V),A0
        else:
            y = self.relu(y)
            y = y.view(N, self.out_channels, -1, self.tau, V // self.tau)
            y = self.out_conv(y).squeeze(dim=3)
            y = self.out_bn(y)
            return y,A0


class ST_block(nn.Module):
    def __init__(self, in_channels, out_channels, num_joints=25, tau=1, num_heads=3, stride=1, dilation=1, autopad=True,
                 residual=True):
        super(ST_block, self).__init__()
        self.s_unit = S_LSU(in_channels, out_channels, num_joints, tau, num_heads)
        self.t_unit = T_LSU(out_channels, out_channels, stride=stride, dilation=dilation, autopad=autopad)
        self.relu = nn.ReLU()

        self.pad = 0
        if not autopad:
            self.pad = (9 - 1) * dilation // 2

        if not residual:
            self.residual = lambda x: 0

        elif (in_channels == out_channels) and (stride == 1):
            self.residual = lambda x: x

        else:
            self.residual = T_LSU(in_channels, out_channels, kernel_size=1, stride=stride)

    def forward(self, x):
        a,b = self.s_unit(x)
        x = self.t_unit(a) + self.residual(x[:, :, self.pad: x.shape[2] - self.pad, :])

        return self.relu(x),b


class UNIK(nn.Module):
    def __init__(self,num_class_object=300, num_class_verbs=97,num_joints=25, num_person=2, tau=1, num_heads=3, in_channels=2):
        super(UNIK, self).__init__()

        self.tau = tau
        self.data_bn = nn.BatchNorm1d(num_person * in_channels * num_joints)



        self.l1 = ST_block(in_channels, 1024, num_joints, tau, residual=False)

        self.l2 = ST_block(1024, 512, num_joints, tau, num_heads, dilation=1)  # 3
        self.l3 = ST_block(512, 512, num_joints, tau, num_heads, dilation=1)  # 3
        self.l4 = ST_block(512, 256, num_joints, tau, num_heads, dilation=1)  # 3
        
        self.l5 = ST_block(256, 256, num_joints, tau, num_heads, stride=1)
        self.l6 = ST_block(256, 256, num_joints, tau, num_heads)
        self.l7 = ST_block(256, 256, num_joints, tau, num_heads)

        self.l8 = ST_block(256, 256, num_joints, tau, num_heads, stride=1)
        self.l9 = ST_block(256, 256, num_joints, tau, num_heads)
        self.l10 = ST_block(256, 256, num_joints, tau, num_heads)
        
        self.fc1 = nn.Linear(256, num_class_object)
        self.fc2 = nn.Linear(num_joints*num_joints, num_class_verbs)
        nn.init.normal_(self.fc1.weight, 0, math.sqrt(2. / num_class_object))
        nn.init.normal_(self.fc2.weight, 0, math.sqrt(2. / num_class_verbs))
        bn_init(self.data_bn, 1)

    def forward(self, x):
        N, C, T, V, M = x.size()

        x = x.permute(0, 4, 3, 1, 2).contiguous().view(N, M * V * C, T)
        x = self.data_bn(x)
        print(np.shape(x),'xxx') 
        x = x.view(N, M, V, C, T).permute(0, 1, 3, 4, 2).contiguous().view(N * M, C, T, V)


        x,_ = self.l1(x)
        x,_ = self.l2(x)
        x,_ = self.l3(x)
        x,_ = self.l4(x)
        x,_ = self.l5(x)
        x,_ = self.l6(x)
        x,_ = self.l7(x)
        x,_ = self.l8(x)
        x,_ = self.l9(x)
        x,adj = self.l10(x) #RUI: I use the adj matrix of last block to predict verbs
        c_new = x.size(1)
        x = x.view(N, M, c_new, -1)
        x = x.mean(3).mean(1)

        return self.fc1(x),self.fc2(adj)

import math
from collections import OrderedDict
import numpy as np
import torch
import torch.nn as nn
from torch.autograd import Variable


class Model(nn.Module):
    def __init__(self, num_class_object=300, num_class_verbs=97, num_joints=353, num_person=2, tau=1, num_heads=3, in_channels=2, drop_out=0,
                 backbone_fixed=False, weights='no'):
        super(Model, self).__init__()
        self.backbone_fixed = backbone_fixed
        self.in_ch = 256
        # load model
        self.model_action = UNIK(352,125, num_joints, num_person, tau, num_heads, in_channels=in_channels)

        if weights != 'no':
            print('pre-training: ', weights)
            weights = torch.load(weights)
            weights = OrderedDict([[k.split('module.')[-1], v.cuda(0)] for k, v in weights.items()])

            keys = list(weights.keys())
            try:
                self.model_action.load_state_dict(weights)
            except:
                state = self.model.state_dict()
                diff = list(set(state.keys()).difference(set(weights.keys())))
                print('Can not find these weights:')
                for d in diff:
                    print('  ' + d)
                state.update(weights)
                self.model.load_state_dict(state)

        # transfer learning
        self.model_action.fc1 = nn.Linear(self.in_ch, num_class_object)

        nn.init.normal_(self.model_action.fc1.weight, 0, math.sqrt(2. / num_class_object))
        # print('parameters: ', len(list(self.model_action.fc.parameters())))
        self.model_action.fc2 = nn.Linear(num_joints*num_joints, num_class_verbs)

        nn.init.normal_(self.model_action.fc2.weight, 0, math.sqrt(2. / num_class_verbs))
        # freeze the backbone for Linear evaluation
        if self.backbone_fixed:
            for l, module in self.model_action._modules.items():
                if l != 'fc':
                    print('fixed layers:', l)
                    for p in module.parameters():
                        p.requires_grad = False

    def get_pad(self):
        return int(self.pad)

    def forward(self, x):
        x1,x2 = self.model_action(x)
        return x1,x2

model = Model(num_class_object=352, num_class_verbs=125, num_joints=354, num_person=1, tau=1, num_heads=3, in_channels=2048+6)


#RUI: Now we start defining what we need for training


import torch
from torch import nn
import torch.nn.functional as F
from torch.nn.parameter import Parameter
import math
import torchvision
from torchvision.ops import roi_align
import torch
from torchvision.ops import RoIAlign
import numpy as np
import torch.nn.functional as F
import torch
import torch
import torch.nn as nn
import torchvision.models as models
from torch.autograd import Variable

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable
import torchvision.models as models

import matplotlib.pyplot as plt
import torchvision
from torchvision import datasets, transforms


import sys

import json
import os
import glob
import cv2
import numpy as np
from random import randint
import torch

import argparse


import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable

import torchvision
from torchvision import datasets, transforms
#from torchsummary import summary

import numpy as np
from barbar import Bar
import pkbar
from apmeter import APMeter



from charades import Charades

from accuracies_action import * #RUI: this is the file we use to get accuracies
from transforms.spatial_transforms import Compose, Normalize, RandomHorizontalFlip, MultiScaleRandomCrop, MultiScaleRandomCropMultigrid, ToTensor, CenterCrop, CenterCropScaled
from transforms.temporal_transforms import TemporalRandomCrop
from transforms.target_transforms import ClassLabel

import warnings
warnings.filterwarnings("ignore")
import time
import torch.hub
import os
import cv2
import numpy as np

from tub_loader20 import * #RUI: here we define the loader we use


dataset = Datasetg(file1='/data/stars/user/mguermal/graphs/graph_work/3')
batch_size = 4
dataloader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=True, num_workers=4, pin_memory=True)



val_dataset = Datasetg(file1='/data/stars/user/mguermal/graphs/graph_work/3/test/')
val_dataloader = torch.utils.data.DataLoader(val_dataset, batch_size=batch_size, shuffle=True, num_workers=4, pin_memory=True)
dataloaders = {'train': dataloader, 'val': val_dataloader}
datasets = {'train': dataset, 'val': val_dataset}









iter_my = []
loss_my = []

accuracy_my_1_v = []
accuracy_my_1_n = []
accuracy_my_1_a = []


accuracy_my_5_v = []
accuracy_my_5_n = []
accuracy_my_5_a = []



model.cuda()
model = torch.nn.DataParallel(model)


lr = 0.01
optimizer = optim.SGD(model.parameters(), lr=lr,momentum=0.9)
lr_sched = optim.lr_scheduler.ReduceLROnPlateau(optimizer, factor=0.1, patience=5, verbose=True)
num_steps_per_update = 1 # accum gradient
steps = 0
max_steps = 100
#lossf = nn.BCEWithLogitsLoss()
lossa = nn.NLLLoss(reduction='mean') #RUI: we use NLLLoss because we predict one verb and one noun
#lossa = nn.CrossEntropyLoss()
m1 = nn.LogSoftmax(dim=1)
while steps<max_steps:
    print('Step {}/{}'.format(steps, max_steps))
    print('-' * 10)
    epoch_time = 0
    for phase in ['train', 'val']:
        if phase == 'train':
            model.train()
            torch.autograd.set_grad_enabled(True)
        else:
            model.train()
            torch.autograd.set_grad_enabled(False)
        total_loss = 0
        iter = 0
        a1 = []
        optimizer.zero_grad()
        if phase == 'train':
            for data in dataloaders[phase]:
                start_iter = time.time()
                iter += 1
                graph, coordinates,  scores ,verb, noun, verbs, nouns = data #RUI: verbs,nouns are one hot vectors while verb and noun are just values( noun = 5 means class 5)
                print(np.shape(verbs),np.shape(nouns))
                print(np.shape(graph),np.shape(coordinates))
                print(np.shape(scores),'scores')
                              
                grap = Variable(graph)
                verb = Variable(verb)
                noun = Variable(noun)
                verbs = Variable(verbs)
                nouns = Variable(nouns)

                verb = torch.from_numpy(np.asarray(verb))
                noun = torch.from_numpy(np.asarray(noun))

                verbs = verbs.squeeze(1)
                verbs = verbs.squeeze(1)
                nouns = nouns.squeeze(1)
                nouns = nouns.squeeze(1)
                strm = time.time()

                full_nodes = graph.unsqueeze(-1)

                output1, output2 = model(full_nodes.float().cuda()) #RUI: here we call the model output2 for verbs and output1 for nouns
                print(np.shape(output1), np.shape(output2))
                
                print(time.time() - strm)

                verbs = verbs.cuda()
                nouns = nouns.cuda()
                verb = verb.cuda()
                noun = noun.cuda()



                


                #RUI: get accuracies and print them to file
                a, b, c, = evaluate_actions(output2.cpu().detach().numpy(),output1.cpu().detach().numpy(),verb.cpu().detach().numpy(),noun.cpu().detach().numpy(),scores[:,0:352].cpu().numpy())

                old_stdout = sys.stdout
                log_file = open("./agcn_train/820/" + phase + "_log_all_%d.log" % steps, "a+")
                sys.stdout = log_file
                print('acc in iter :', iter, ' in ', phase)
                print(a, b, c)
                #RUI: get losses and print them to file
                loss1 = lossa(m1(output2), verb)
                loss2 = lossa(m1(output1), noun)

                loss = 0.5 * loss1 + 0.5 * loss2
                total_loss += loss.item()
                print('loss in iter :', iter, ' in ', phase)
                print(loss)
                print('---------------------------------------\n')
                time_iter = time.time() - start_iter
                epoch_time += time_iter

                #RUI: here i sum accuracies of each iter
                if len(a1) > 0:
                    tot_acc_verbs += np.asarray(a)
                    tot_acc_objects += np.asarray(b)
                    tot_acc_action += np.asarray(c)
                else:
                    tot_acc_verbs = np.asarray(a)
                    tot_acc_objects = np.asarray(b)
                    tot_acc_action = np.asarray(c)
                    a1 = [1]
                #RUI : here backprop
                if phase == 'train':
                    loss.backward()
                    optimizer.step()
                    optimizer.zero_grad()

                sys.stdout = old_stdout

                log_file.close()

               
                


            #RUI: following lines are just some plots u can ignore and go see validation process
            iter_my.append(steps)
            loss_my.append(total_loss / iter)
            accuracy_my_1_v.append(tot_acc_verbs[0] / iter)
            accuracy_my_1_n.append(tot_acc_objects[0] / iter)
            accuracy_my_1_a.append(tot_acc_action[0] / iter)

            accuracy_my_5_v.append(tot_acc_verbs[1] / iter)
            accuracy_my_5_n.append(tot_acc_objects[1] / iter)
            accuracy_my_5_a.append(tot_acc_action[1] / iter)

            save_model = './gcn_checkpoints_all0/agcn_train/820/epic_kitchen_gcn_'
            torch.save(model.module.state_dict(), save_model + str(steps).zfill(6) + '.pt')


            old_stdout = sys.stdout
            log_file = open("./agcn_train/820/" + phase + "_log_all_%d.log" % steps, "a+")
            sys.stdout = log_file

            plt.plot(iter_my, loss_my)
            plt.title("train Loss Curve")
            plt.xlabel("epoch")
            plt.ylabel("Loss")
            if os.path.exists(
                    '/data/stars/user/mguermal/model_2/models/X3D-Multigrid/agcn_train/820/plots/loss/') == False:
                os.mkdir('/data/stars/user/mguermal/model_2/models/X3D-Multigrid/agcn_train/820/plots/loss/')

            plt.savefig(
                '/data/stars/user/mguermal/model_2/models/X3D-Multigrid/agcn_train/820/plots/loss/loss%03d.png' % steps)
            # plt.show()
            plt.close()
            # plotting Accuracy curve
            plt.plot(iter_my, accuracy_my_1_v)
            plt.title("Training accuracy Curve ")
            plt.xlabel("Iterations")
            plt.ylabel("Training Accuracy")
            if os.path.exists(
                    '/data/stars/user/mguermal/model_2/models/X3D-Multigrid/agcn_train/820/plots/accv/') == False:
                os.mkdir('/data/stars/user/mguermal/model_2/models/X3D-Multigrid/agcn_train/820/plots/accv/')
            plt.savefig(
                '/data/stars/user/mguermal/model_2/models/X3D-Multigrid/agcn_train/820/plots/accv/accuracy_1%03d.png' % steps)
            # plt.show()
            plt.close()

            plt.plot(iter_my, accuracy_my_5_v)
            plt.title("Training accuracy Curve ")
            plt.xlabel("Iterations")
            plt.ylabel("Training Accuracy")
            plt.savefig(
                '/data/stars/user/mguermal/model_2/models/X3D-Multigrid/agcn_train/820/plots/accv/accuracy_5%03d.png' % steps)
            # plt.show()
            plt.close()

            plt.plot(iter_my, accuracy_my_1_n)
            plt.title("Training accuracy Curve ")
            plt.xlabel("Iterations")
            plt.ylabel("Training Accuracy")
            if os.path.exists(
                    '/data/stars/user/mguermal/model_2/models/X3D-Multigrid/agcn_train/820/plots/accn/') == False:
                os.mkdir('/data/stars/user/mguermal/model_2/models/X3D-Multigrid/agcn_train/820/plots/accn/')
            plt.savefig(
                '/data/stars/user/mguermal/model_2/models/X3D-Multigrid/agcn_train/820/plots/accn/accuracy_1%03d.png' % steps)
            # plt.show()
            plt.close()

            plt.plot(iter_my, accuracy_my_5_n)
            plt.title("Training accuracy Curve ")
            plt.xlabel("Iterations")
            plt.ylabel("Training Accuracy")
            plt.savefig(
                '/data/stars/user/mguermal/model_2/models/X3D-Multigrid/agcn_train/820/plots/accn/accuracy_5%03d.png' % steps)
            # plt.show()
            plt.close()

            print('total train loss epoch %d is :' % steps, total_loss / iter)
            print('total train acc epoch %d is :' % steps, tot_acc_verbs / iter,tot_acc_objects / iter, tot_acc_action/iter)
            print('time for epoch train set:', epoch_time)
            print('--------------------------------------------------')

            sys.stdout = old_stdout

            log_file.close()
        
        if phase == 'val':
            iter = 0
            with torch.no_grad(): #RUI: to optimise validation tima and memory the rest of the process is similar but with no backprob
                for data in dataloaders[phase]:


                    start_iter = time.time()
                    iter += 1


                    graph, coordinates,scores, verb, noun, verbs, nouns = data

                    graph = Variable(graph)
                    verb = Variable(verb)
                    noun = Variable(noun)
                    verbs = Variable(verbs)
                    nouns = Variable(nouns)



                    verb = torch.from_numpy(np.asarray(verb))
                    noun = torch.from_numpy(np.asarray(noun))
                    verbs = verbs.squeeze(1)
                    verbs = verbs.squeeze(1)
                    nouns = nouns.squeeze(1)
                    nouns = nouns.squeeze(1)




                    strm = time.time()
                    full_nodes = graph.unsqueeze(-1)
                    output1, output2 = model(full_nodes.float().cuda())
                    print(np.shape(full_nodes))
                    print(np.shape(output2), np.shape(output1))



                    verbs = verbs.cuda()
                    nouns = nouns.cuda()
                    verb = verb.cuda()
                    noun = noun.cuda()

                    print(nouns[0], verbs[0])




                    m = nn.Softmax(dim=1)


                    a, b, c, = evaluate_actions(output2.cpu().detach().numpy(),output1.cpu().detach().numpy(),verb.cpu().detach().numpy(),noun.cpu().detach().numpy(),scores[:,0:352].cpu().numpy())

                    old_stdout = sys.stdout


                    old_stdout = sys.stdout
                    log_file = open("./agcn_train/820/" + phase + "_log_all_%d.log" % steps, "a+")
                    sys.stdout = log_file
                    print('acc in iter :', iter, ' in ', phase)
                    print(a, b, c)
                    
                    loss1 = lossa(m1(output2), verb)
                    loss2 = lossa(m1(output1), noun)
                    loss = loss1*0.5 + loss2*0.5

                    total_loss += loss.item()
                    print('loss in iter :', iter, ' in ', phase)
                    print(loss)
                    time_iter = time.time() - start_iter
                    epoch_time += time_iter
                    print('---------------------------------------\n')
                    if len(a1) > 0:
                        tot_acc_verbs += np.asarray(a)
                        tot_acc_objects += np.asarray(b)
                        tot_acc_action += np.asarray(c)
                    else:
                        tot_acc_verbs = np.asarray(a)
                        tot_acc_objects = np.asarray(b)
                        tot_acc_action = np.asarray(c)
                        a1 = [1]
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()
                        optimizer.zero_grad()

                    sys.stdout = old_stdout

                    log_file.close()

            lr_sched.step(total_loss / iter)


            old_stdout = sys.stdout
            log_file = open("./agcn_train/820/" + phase + "_log_all_%d.log" % steps, "a+")
            sys.stdout = log_file
            print('total val acc epoch %d is :' % steps, tot_acc_verbs / iter,tot_acc_objects / iter,tot_acc_action/iter)
            print('total val loss epoch %d is :' % steps, total_loss / iter)
            print('train and val time :', epoch_time)
            print(
                '----------------------------------------------------------------------------------------------------------\n')
            print('---------------new epoch start----------------')
            sys.stdout = old_stdout

            log_file.close()

            steps += 1



