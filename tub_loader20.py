import json
import os
import glob
import cv2
import numpy as np
import pickle

import pandas as pd
import torch
import torch.utils.data as data_utl

import torchvision
from torchvision import datasets, transforms
import random


import os
import glob
import cv2
import numpy as np
from random import randint
import torch

import argparse


import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.optim import lr_scheduler
from torch.autograd import Variable

import torchvision
from torchvision import datasets, transforms
#from torchsummary import summary

import numpy as np
from barbar import Bar
import pkbar
from apmeter import APMeter





from transforms.spatial_transforms import Compose, Normalize, RandomHorizontalFlip, MultiScaleRandomCrop, MultiScaleRandomCropMultigrid, ToTensor, CenterCrop, CenterCropScaled
from transforms.temporal_transforms import TemporalRandomCrop
from transforms.target_transforms import ClassLabel

import warnings


import time

















def make_dataset(file1):
    dataset = []
    #RUI: in files1 stored nodes and scores and coordinates
    for elm in os.listdir(os.path.join(file1,'nodes')):
        clip = elm.split('.')[0]
        clip2 = clip.split('_')
        video_id = clip2[0]+'_'+clip2[1]
        start_frame = clip2[2]
        end_frame = clip2[3]
        verb = int(clip2[4])
        noun = int(clip2[5])
        action = clip2[6]
        verbs = np.zeros((1,125))
        nouns = np.zeros((1,352))
        verbs[0][int(verb)] = 1
        nouns[0][int(noun)] = 1
        dataset.append((video_id,elm,verb,noun,verbs,nouns,action))
    return dataset
def load_clip(file1,clip,video_id,indxs):
    strt = time.time()
    path1 = os.path.join(file1,'nodes')
    path2 = os.path.join(file1,'class_cores')
    path3 = os.path.join(file1,'class_coord')
    path4 = os.path.join(file1,'scores')
    vec_nodes = np.load(os.path.join(path1,clip),allow_pickle=True, encoding='latin1')
    vec_class = np.load(os.path.join(path2,clip),allow_pickle=True, encoding='latin1')
    vec_coord = np.load(os.path.join(path3,clip),allow_pickle=True, encoding='latin1')
    print(clip)
    if file1=='/data/stars/user/mguermal/graphs/graph_work/3/test/':
        vec_scores = np.load(os.path.join('/data/stars/user/mguermal/graphs/graph_work/test/scores',clip),allow_pickle=True, encoding='latin1')
    else:
        vec_scores = np.load(os.path.join('/data/stars/user/mguermal/graphs/graph_work/scores',clip),allow_pickle=True, encoding='latin1')
    print('here')
    graph = np.zeros((354,128,2048))
    sub_set = random.randint(0,4)#RUI: for each clip stored 4 different nodes each with it s one start frame
    coordinates = vec_coord[sub_set]
    #RUI: here I concat nodes with coordinates
    if video_id == 'P12_01' or video_id == 'P12_02' or video_id == 'P12_03':
        vec = [0,0,1,1,1,1]
        vec = np.tile(vec, (128, 1))
        vec = np.expand_dims(vec,0) 
        coordinates = np.concatenate((coordinates,vec),0)
    elif video_id == 'P12_05' or video_id == 'P12_06':
        vec = [0,0,1,1,1,1]
        vec = np.tile(vec, (128, 1))
        vec = np.expand_dims(vec,0)
        coordinates = np.concatenate((coordinates,vec),0)
    else:
        vec = [0,0,1,1,1,1]
        vec = np.tile(vec, (128, 1))
        vec = np.expand_dims(vec,0)
        

        coordinates = np.concatenate((coordinates,vec),0)
    
    for i in range(len(vec_class[sub_set])):
        object = vec_class[sub_set][i]
        
        sub_graph = vec_nodes[sub_set][i]
        graph[int(object)] = sub_graph
    
   

    graph = torch.from_numpy(graph)
    n,t,d = graph.size()


    graph = graph.permute(2,1,0)
    

    
    coordinates = torch.from_numpy(coordinates)
    n,t,c = coordinates.size()

    coordinates = coordinates.permute(2,1,0)
    graph = torch.cat((graph,coordinates),0)
    vec_scores = torch.from_numpy(vec_scores)
    
    
    indxs = np.asarray(indxs)
    d,t,n = graph.size()
    
    g = np.zeros((d,16,n))
    
    cord = np.zeros((c,16,n))
    #RUI: I added this part to try different time steps on the nodes the extracted nodes are on 128 frame and here we can sub-sample
    for i in range(len(indxs)):
        g[:,i,:] = graph[:,indxs[i],:]
        cord[:,i,:] = coordinates[:,indxs[i],:]
    g = torch.from_numpy(g)
    cord = torch.from_numpy(cord)
    







    return g,cord,vec_scores


class Datasetg(data_utl.Dataset):
    def __init__(self, file1):
        self.file1 = file1
        self.data = make_dataset(self.file1)
        self.transform = transforms.Compose([transforms.ToTensor()])
        

    def __getitem__(self, index):
        s = 128-16
        start = random.randint(0, s)
        k = 0
        indxs = []
        for i in range(16):

            indxs.append(start + k*1 )

            k = k + 1
            if indxs[-1] >= 128 - 1:
                k = 0
        video_id, clip,verb,noun,verbs,nouns,action = self.data[index]

        graph,coordinates,scores = load_clip(self.file1,clip,video_id,indxs)

        verbs = torch.from_numpy(verbs)
        nouns = torch.from_numpy(nouns)


        return graph,coordinates,scores,verb,noun,verbs,nouns

    def __len__(self):
        return len(self.data)







